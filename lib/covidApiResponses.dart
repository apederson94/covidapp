class CovidTotalResponse {
  CovidData data;
  String dateTime;
  double timestamp;

  CovidTotalResponse.fromJson(Map<String, dynamic> parsedJson) {
    Map<String, dynamic> totalCases = parsedJson;

    this.data = CovidData.fromJson(totalCases['data']);
    this.data.location = "Global";
    this.dateTime = totalCases['dt'];
    this.timestamp = totalCases['ts'];
  }
}

class CovidCurrentResponse {
  List<CovidData> allData;
  String dateTime;
  double timestamp;

  CovidCurrentResponse.fromJson(Map<String, dynamic> parsedJson) {
    for (var countryData in parsedJson['data']) {
      this.allData.add(CovidData.fromJson(countryData));
    }

    this.dateTime = parsedJson['dt'];
    this.timestamp = parsedJson['ts'];
  }
}

class CovidSingleCountryResponse {
  CovidData data;
  String dateTime;
  double timestamp;

  CovidSingleCountryResponse.fromJson(Map<String, dynamic> parsedJson) {
    Map<String, dynamic> totalCases = parsedJson;

    this.data = CovidData.fromJson(totalCases['data']);
    this.dateTime = totalCases['dt'];
    this.timestamp = totalCases['ts'];
  }
}

class CovidData {
  String location;
  int confirmed;
  int deaths;
  int recovered;
  int active;

  CovidData.fromJson(Map<String, dynamic> parsedJson) {
    this.location = parsedJson['location'];
    this.confirmed = parsedJson['confirmed'];
    this.deaths = parsedJson['deaths'];
    this.recovered = parsedJson['recovered'];
    this.active = parsedJson['active'];
  }
}