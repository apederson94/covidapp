import 'dart:convert';

import 'package:covidApp/covidApiResponses.dart';
import 'package:http/http.dart' as http;

String apiBase = 'https://covid2019-api.herokuapp.com/v2/';
String latestCases = apiBase + 'total';

class CovidApi {
	static Future<CovidTotalResponse> getAllData() async {
		final response = await http.get(latestCases);

		if (response.statusCode == 200) {
			return CovidTotalResponse.fromJson(json.decode(response.body));
		} else {
			return null;
		}
	}

}