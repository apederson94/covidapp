
import 'dart:math';
import 'dart:ui';

import 'package:covidApp/covidApiResponses.dart';
import 'package:flutter/material.dart';

class CovidCasesGraph extends CustomPainter{
  CovidTotalResponse dataPoints;
  double recoveredRadians;
  double deadRadians;
  double activeRadians;
  double unknownRaidans;

  Color recoveredColor;
  Color deadColor;
  Color activeColor;
  Color unknownColor;

  CovidCasesGraph.fromLatestData(CovidTotalResponse newData) {
    dataPoints = newData;
    int total = dataPoints.data.confirmed;
    int active = dataPoints.data.active;
    int deaths = dataPoints.data.deaths;
    int recovered = dataPoints.data.recovered;

    double percentRecovered = recovered/total;
    double percentDead = deaths/total;
    double percentActive = active/total;
    double percentUnknown = 1.0 - (percentRecovered + percentDead + percentActive);

    recoveredRadians = _convertPercentToRadians(percentRecovered);
    deadRadians = _convertPercentToRadians(percentDead);
    activeRadians = _convertPercentToRadians(percentActive);
    unknownRaidans = _convertPercentToRadians(percentUnknown);
  }

  double _convertPercentToRadians(double percent) {
    final double radiansInCircle = 2 * pi;

    return percent * radiansInCircle;
  }

  int random255() {    
    return Random.secure().nextInt(255);
  }

  Color drawSlice(Canvas canvas, Rect rect, double startAngle, double angleToDraw) {
    Paint paint = Paint();
    paint.color = Color.fromARGB(255, random255(), random255(), random255());

    canvas.drawArc(rect, startAngle, angleToDraw, true, paint);
    print("startAngle: $startAngle, endAngle: $angleToDraw, color: ${paint.color}");

    return paint.color;
  }

  @override
  void paint(Canvas canvas, Size size) {
    Rect rect = Rect.fromLTWH(0, 0, size.width, size.height);
    double startPosition = 0;

    recoveredColor = drawSlice(canvas, rect, startPosition, recoveredRadians);
    startPosition += recoveredRadians;
    
    deadColor = drawSlice(canvas, rect, startPosition, deadRadians);
    startPosition += deadRadians;
    
    activeColor = drawSlice(canvas, rect, startPosition, activeRadians);
    startPosition += activeRadians;
    
    unknownColor = drawSlice(canvas, rect, startPosition, unknownRaidans);
    
    Rect innerRect = Rect.fromCenter(center: rect.center, width: rect.width * 0.75, height:  rect.height * 0.75);
    Paint innerPaint = Paint();
    innerPaint.color = Color.fromARGB(255, 255, 255, 255);
    canvas.drawArc(innerRect, 0, 2*pi, true, innerPaint);

    canvas.save();
    canvas.restore();
  }
  
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}